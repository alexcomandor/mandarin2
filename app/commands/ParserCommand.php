<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Sunra\PhpSimple\HtmlDomParser;

class ParserCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:parser';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'We will parce the data and write in db';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		
		//
		// $this->info('Display this on the screen');

		// Получаем данные курса валют
		$curl = new Curl\Curl();
		$curl->get('http://www.banki.ru/products/currency/cb/');

		if ($curl->error) {
		    $this->error($curl->error_code);
		    die();
		}
		
		$query = phpQuery::newDocumentHTML($curl->response);
		$dollar = pq('#currency-USD > td:nth-child(4)')->text();
		$euro = pq('#currency-EUR > td:nth-child(4)')->text();
		$currency = new Currency;

		if(date("Y-m-d") == Currency::orderBy('id', 'DESC')->first()->date){
				$this->info("Сегодня уже снимали курс");			
		}else{
			$currency->dollar = str_replace(',', '.',$dollar);
			$currency->euro = str_replace( ',', '.',$euro);
			$currency->date=date("Y-m-d");
			$currency->save();
			$this->info("Курсы валют успешно обновлены");
		}
		$curl->close();
		unset($query);


		// Получаем топ новостей из газета.ру
		// http://www.gazeta.ru/export/rss/lastnews.xml
		$curl = new Curl\Curl();
		$curl->get('http://www.gazeta.ru/export/rss/lastnews.xml');

		if ($curl->error) {
		    $this->error($curl->error_code);
		    die();
		}

		$query = phpQuery::newDocumentXML($curl->response);
		$items = $query->find('item');


			foreach ($items as $item) {
				$news = new News;

				$news->link = pq($item)->find('link')->text();

				$this->getArticle($news->link);
			}
			$curl->close();
			// unset($query);

		$this->getGoroskop();
	}

		public function getArticle($url){
			
			$curl = new Curl\Curl();
			$curl->get($url);
			$news = new News;

			$dom = HtmlDomParser::str_get_html( $curl->response);
			$title = $dom->find('article h1');
			$content = $dom->find('.text');

			$news->title = $title[0]->plaintext;
			$news->content = strip_tags($content[0]->innertext, '<p><b><strong><br>');
			$alias = str_replace(array('raquo','laquo' ), '', $news->title);
			$news->alias = Str::limit(Str::slug($this->rus2translit($alias), '_'), 80,'');

			$this->info($news->alias);

			if (empty(News::wherealias($news->alias)->first()->alias)){
				$news->save();
				$this->info('Новость успешно добавлена');
			}else{
				$this->info("Эта новость уже есть");
			}
			
			unset($dom);
			$curl->close();
		}

	public function getGoroskop(){

		$znaki = array("aries","taurus","gemini","cancer","leo","virgo","libra","scorpio","sagittarius","capricorn","aquarius","pisces");

		if(date("Y-m-d") == Horos::orderBy('id', 'DESC')->first()->date){
			$this->info('Сегодня уже брали гороскоп');

		}else{
		foreach ($znaki as $znak) {
			$horo = new Horos;

			$curl = new Curl\Curl();

			$curl->get("http://www.goroskop.ru/publish/open_article/29/".$znak.'/');
			$dom = HtmlDomParser::str_get_html( $curl->response);

			$horo->content = $dom->find('#article',0)->children(1)->outertext;
			$horo->title = $dom->find('#gContent',0)->first_child()->outertext;
			$horo->title = strip_tags($horo->title,"<span>");
			// $title = $dom->find('h1');
			// $horo->title = $title[0]->innertext;

			$horo->date=date("Y-m-d");
			$horo->zodiak = $znak;
			$horo->save();

			$this->info($znak);
			// $this->info($content);
			 unset($dom);
			 $curl->close();
		}
	}
		
}

	public function rus2translit($string) {
    $converter = array(
        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'y',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'h',   'ц' => 'c',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
        'ь' => '',  'ы' => 'y',   'ъ' => '',
        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
        //'&laquo;'=>'', '&raquo;'=>'',
        
        'А' => 'A',   'Б' => 'B',   'В' => 'V',
        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
        'И' => 'I',   'Й' => 'Y',   'К' => 'K',
        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
        'О' => 'O',   'П' => 'P',   'Р' => 'R',
        'С' => 'S',   'Т' => 'T',   'У' => 'U',
        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
        'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
    );
    return strtr($string, $converter);
}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('example', InputArgument::OPTIONAL, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
