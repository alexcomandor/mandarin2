<?php

class SearchController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /search
	 *
	 * @return Response

	 */

	public $data;

	public function index()
	{

		if(Input::get('text')){
        	return Redirect::to('http://www.google.com/search?q='.Input::get('text'));
    	}
	}


	public function news(){
		$all = News::take(5)->orderBy('id', 'DESC')->skip(10)->get();
		return $all->reverse();

	}

	public function goroscop(){
		$all = Horoscop::all();
		return $all;
	}

	public function aneks(){
		$all = Anek::all()->random(2);
		return $all;
	}


	public function search(){
		$data['news'] = $this->news();
		$data['goroscop'] = $this->goroscop();
		$data['aneks'] = $this->aneks();


		// dd($data['goroscop']);
		// $data = News::all();
		return  View::make('main.search')->with(array('data'=> $data)
		);
	}


	public function zodiac($name){
		return $name;
	}


	public function singlenew($name){
		// return $name;

		$data = array();
		$data['newsAll'] = $this->news();
		$data['news'] = News::wherealias($name)->first();
		return  View::make('news.news')->with(array('data'=> $data, 'title' => $data['news']->title)
		);
	}

	public function goroOne(){
		$data = array();
		$data['horo'] = Horos::all();
		$today = date("Y-m-d");

		$data['horo'] = Horos::where('date', '=',  $today)->get();
		return  View::make('news.horoscop')->with(array('data'=> $data, 'title' => 'Ваш гороскоп на сегодня!')
		);
	}

	
}