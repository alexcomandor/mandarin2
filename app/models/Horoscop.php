<?php

class Horoscop extends \Eloquent {
	protected $fillable = [];
	protected $table = 'goroskop';

	public function horos()
    {
        return $this->hasOne('Horos');
    }
}