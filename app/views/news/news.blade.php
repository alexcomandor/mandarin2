@extends('layouts.basic')

@section('content')
@include('ads.720x90')
@include('main.searchbar')

<h1>{{ $data['news']->title }}</h1>
<div class="text">
	<div style="align:left;">
	@include('ads.240x400')
	</div>
	
	<?php 

$adsense = <<<HTML
<div style="float:right"><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> 
<!-- Mandarin_300x250 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:250px"
     data-ad-client="ca-pub-1567683945644169"
     data-ad-slot="6607486333"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script> </div>
HTML;

		$text  = $data['news']->content;
		$content = preg_replace('@([^^]{200}.*?)(\r?\n\r?\n|</p>)@', "\\1$adsense\\2", trim($text), 1); 
		// $content = substr_replace($text,mb_substr($text,0,800).$ad,0,5);
		echo $content;
		// echo $text;

	 ?>
<div class="similar">
	
<h3> Похожие новости </h3>
		<ol>
			@foreach ( $data['newsAll'] as $key)
				<li> <a href="{{$key['alias']}}">{{ $key['title'] }}</a> </li>
			@endforeach
		</ol>
</div>

</div>
@stop
