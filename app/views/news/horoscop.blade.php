@extends('layouts.basic')

@section('content')
@include('ads.720x90')
@include('main.searchbar')


<h1>{{ $title }}</h1>

<div class="row">
	<div class="col-md-9">
		
		@for ( $i = 1; $i<12; $i++)
			@if ( $i == 2)
				@include('ads.468x15')
			@endif
			<h3><a name="{{ $data['horo'][$i]->zodiak}}">{{ $data['horo'][$i]->title}}</a></h3>
			{{ $data['horo'][$i]->content}}
		@endfor

		
		
	</div>
	<div class="col-md-3">
		@include('ads.300x250')
		@include('ads.240x400')
	</div>
</div>
@stop
