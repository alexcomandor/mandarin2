<!doctype html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"> -->
	<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
	{{ HTML::style('css/bootstrap.min.css') }}
	{{ HTML::style('css/style.css') }}
	<title> {{ $title or 'Поисковая система 3 поколения' }}  </title>
</head>
<body>

	<div class="container">
		@yield('head')
		@yield('content')
	</div>

	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script> -->
	{{ HTML::script('js/bootstrap.min.js')}}
	{{ HTML::script('js/jquery.js')}}
</body>
</html>

