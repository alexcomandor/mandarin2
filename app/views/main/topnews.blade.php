	<div id="wide"></div>
<div class="row topnews" >

	<div class="col-md-6">
		<h3> Новости дня </h3>
		<ol>
			@foreach ( $data['news'] as $key)
				<li> <a href="news/{{$key['alias']}}">{{ $key['title'] }}</a> </li>
			@endforeach
		</ol>
		@include('ads.468x15')
	</div>

	<div class="col-md-4">
		<div class="ad300">
			@include('ads.300x250')
		</div>
	</div>
	<div class="col-md-2 wide">
		<h4>Москва</h4>
		<p>	{{Currency::orderBy('id', 'DESC')->first()->date}}</p>

		<h4>Курс валют</h4>
		<p><strong>USD</strong>- {{Currency::orderBy('id', 'DESC')->first()->dollar}}</p>
		<p><strong>EUR</strong>- {{Currency::orderBy('id', 'DESC')->first()->euro}}</p>

		<h4> Товар дня </h4>
		<p><a href="http://s.kma1.biz/kCURD2/BeatsSoloHD_mand::">Наушники Beats Solo HD</a></p>
	</div>

</div>