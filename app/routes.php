<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Route::get('/', function()
// {
// 	return View::make('main.search');
// });

Route::get('/', array('uses'=>'SearchController@search', 'as'=>'search'));

Route::post('/', array('uses'=>'SearchController@index', 'as'=>'go'));
Route::get('goroscop/{name}', array('uses'=>'SearchController@goroOne', 'as'=>'goroskop'));
Route::get('goroscop', array('uses'=>'SearchController@goroOne', 'as'=>'goroskop'));
Route::get('news/{name}', array('uses'=>'SearchController@singlenew', 'as'=>'goroskop'));
Route::get('search', function(){
	return View::make('news.search');
});

// Route::controller('search', 'SearchController@index');
